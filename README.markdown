﻿Sample compilation script using MSBuild. This is for testing only.to check it is triggering ci/cd pipeline
and pushing it to nexus and ucd version import 
The script performs the following steps:

1. Build the solution
2. Run unit tests
3. Create a nuget package

Just run `build.cmd`. The results will be stored in `.\results\` folder.

