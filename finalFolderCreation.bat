@echo off
    setlocal enableextensions EnableDelayedExpansion
	
	for /f %%i in (test.txt) do (	
		svn checkout %%i
		call :changeDirectory %%i newFolderPath
		
		call :createFolderStructure !newFolderPath! folderStructure
		
	)	
	
	endlocal &goto :eof
	
:changeDirectory
	setlocal enableextensions
	set NAME=%1
	for /f "delims=" %%i in ("%1") do set "folderName=%%~ni"
	cd %folderName%
	set folderPath=%cd%
	endlocal&set %2=%folderPath%&goto :eof

	
:createFolderStructure
	setlocal enableextensions disabledelayedexpansion
	
	cd %1
	echo the path is %cd%
	FOR /F %%i IN ('dir /A:D-H /T:C /O:-D /B') DO (
				 
    SET a=%%i

    GOTO :found_last 
	)
	goto :eof

:found_last
	setlocal enableextensions enabledelayedexpansion
	
	echo Most recent subfolder: %a%
	set last_subforlder=%a%
	call :extractLeadingText %last_subforlder% leadingText
	call :extractLeadingNumbers %last_subforlder% leadingNumbers
	set /a startingNumber=%leadingNumbers%+1
	
	
	FOR /L %%s IN (%leadingNumbers%,1,1000) DO (
			set /a today=%%s
			echo today is !today!
			set /a tomorrow = !today!+2
			echo tomorrow is !tomorrow!
			if exist %leadingText%%%s (
				echo file exists
				echo %leadingText%%%s
			) else (
				echo in the else block the file name should be %leadingText%%%s
				
				set /a "newNumber=%%s"
				echo The new number is !newNumber!	
				set k=!newNumber!+2
				echo the value is %k%
				set /a "finalNumber=!newNumber!+2"
				echo the final number is !tomorrow!
				for /L %%x IN (%%s,1,!tomorrow!) do (
				echo leadig number is %%x
				echo tomorrow is today's tomorrow !tomorrow!
				echo leasing text is %leadingText%
				
				md %leadingText%%%x %leadingText%%%x\APP %leadingText%%%x\RELEASENOTES %leadingText%%%x\DB\ROLLABCK %leadingText%%%x\DB\SCRIPT
				
						svn add %leadingText%%%x
						svn commit -m "added new folders with batch script"
						echo folder creation
				)
				
				
				echo came back  
				goto :eof
			)
		)
	GOTO :eof
	
rem This extracts the first numerical series in the input string    
:extractLeadingText inputString returnVar
    setlocal enableextensions disabledelayedexpansion
    rem Retrieve the string from arguments
    set "string=%~1"

    rem Use numbers as delimiters (so they are removed) to retrieve the rest of the string
    for /f "tokens=1-2 delims=.0123456789 " %%a in ("%string:^"=%") do set "delimiters=%%a%%b"
	
    rem Return the found data to caller and leave
    endlocal & set "%~2=%delimiters%"
	
    goto :eof
	
rem This extracts the first numerical series in the input string    
:extractLeadingNumbers inputString returnVar
    setlocal enableextensions disabledelayedexpansion
    rem Retrieve the string from arguments
    set "string=%~1"

    rem Use numbers as delimiters (so they are removed) to retrieve the rest of the string
    for /f "tokens=1-2 delims=0123456789 " %%a in ("%string:^"=%") do set "delimiters=%%a%%b"
	
    rem Use the retrieved characters as delimiters to retrieve the first numerical serie
    for /f "delims=%delimiters% " %%a in ("%string:^"=%") do set "numbers=%%a"

    rem Return the found data to caller and leave
    endlocal & set "%~2=%numbers%"
    goto :eof